package exo19;

import java.lang.reflect.InvocationTargetException;

public class Exo19 {

	@SuppressWarnings("unused")
	public static void main(String[] args) 
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, 
			NoSuchFieldException, SecurityException, 
			NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		
		AnalyzeBean beanAnalyzer = new AnalyzeBean();
		
		Person person = new Person("Paul","liio", 33);
		
		Class<?> clss1 = person.getClass();
		Class<?> clss2 = Person.class;
		Class<?> clss3 = Class.forName("exo19.Person");
		System.out.println("clss1 == clss2 " + (clss1 == clss2));
		System.out.println("clss1 == clss3 " + (clss1 == clss3));
		
		Object newInstance = clss1.getConstructor().newInstance();
		System.out.println("new instance = " + newInstance);
		
		Person p = (Person)beanAnalyzer.getInstance("exo19.Person");
		System.out.println("p = " + p);
	
		PersonReader pr= new PersonReader();

		System.out.println(pr.read("files/persons.txt"));
	}

}
